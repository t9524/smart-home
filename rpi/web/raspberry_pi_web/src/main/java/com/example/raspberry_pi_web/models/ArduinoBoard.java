package com.example.raspberry_pi_web.models;


import java.io.*;
import java.util.ArrayList;

public class ArduinoBoard {

    static long lastTime = 0;
    static long lastTime2 = 0;

    public int sendCommand(String command) throws IOException,InterruptedException {
        try (FileWriter writer = new FileWriter("/dev/ttyUSB0", true)) {
            // запись всей стро
            writer.write(command);
            // запись по символам
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }
  /*  private int fileStart(String command)throws IOException, InterruptedException{
        lastTime = System.nanoTime();

            //Process proc = Runtime.getRuntime().exec("sudo ../resources/connector" + command);//sudo ../c_connector/src/test 66

            proc.waitFor();

            int devilString = proc.exitValue();
            System.out.println("We received exit code: " + devilString);

            proc.destroy();
        System.out.println("\tTime of reading: " + (System.nanoTime()-lastTime));
        lastTime = System.nanoTime();
        return  devilString;
    }
    */
    public String getAnswer() throws IOException {
        //ArrayList<String> strings = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream("/dev/ttyUSB0");
             InputStreamReader isr = new InputStreamReader(fis);
             BufferedReader br = new BufferedReader(isr)) {
            //String str;
            //while ((str = br.readLine()) != null) {
                //strings.add(str);
           // }
            return br.readLine();
        }
    }
}
