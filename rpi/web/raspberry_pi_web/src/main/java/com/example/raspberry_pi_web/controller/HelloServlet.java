package com.example.raspberry_pi_web.controller;

import com.example.raspberry_pi_web.models.ArduinoBoard;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet  {
    private String message;

    ArduinoBoard arduino = new ArduinoBoard();
    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String path = request.getParameter("action");
        if (path == null) path = "terminal";
        switch (path) {
            case "led_on":
                led1Control(request, response, true);
                break;
            case "led_off":
                led1Control(request, response, false);
                break;
            case "terminal":
                commandManager(request, response);
                break;
            default:
                out.println("<h1>" + "error!!!" +"</h1>");
                out.println("<html><body>");
        }
    }

    public void destroy() {
    }
    protected void led1Control(HttpServletRequest request, HttpServletResponse response, boolean mode) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            if(mode){
                arduino.sendCommand(">led_1_ON");
                wait(40);
                String result = arduino.getAnswer();
                if(result.equals(">always is ok!<")){
                    out.println("<h1>" + "led 1 on -- " + result +"</h1>");
                }else{
                    out.println("<h1>" + "led 1 status unknown -- " + result +"</h1>");
                }
            }else{
                arduino.sendCommand(">led_1_OFF");
                wait(40);
                String result = arduino.getAnswer();
                if(result.equals(">always is ok!<")){
                    out.println("<h1>" + "led 1 off -- " + result +"</h1>");
                }else{
                    out.println("<h1>" + "led 1 status unknown -- " + result +"</h1>");
                }
            }
            out.println("<html><body>");

        } catch(Exception e){
            out.println("<h1>" + e.getStackTrace() + "<->" + e.getMessage() + "<->" + e.toString() +"<->" +  e.getCause() +"</h1>");
        }
        out.println("</body></html>");
    }
    protected void commandManager(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try {
            String inputCommand = request.getParameter("inputCommand");
            arduino.sendCommand(">" + inputCommand);
            wait(40);
            String result = arduino.getAnswer();
                out.println("<h1>" + result + "</h1>");
        } catch(Exception e){
            out.println("<h1>" + e.getStackTrace() + "<->" + e.getMessage() + "<->" + e.toString() +"<->" +  e.getCause() +"</h1>");
        }
        out.println("</body></html>");
    }
    public static void wait(int ms) {
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
}