<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
    <h1><%= "Hello World!" %>
    </h1>
    <br/>
    <a href="hello-servlet?action=led_on">ON</a>
    <a href="hello-servlet?action=led_off">OFF</a>
    <form class="login-form" action="hello-servlet?action=terminal" >
        <table>
            <tr><td>command:<input type="text" name="inputCommand" size="10"></td></tr>
            <td><input type="submit" value="send"></td>
        </table>
    </form>
</body>
</html>