#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/slab.h>
#include <linux/ctype.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("IOT system group");
MODULE_DESCRIPTION("Kernel module to communicate with mcu controllers");

#define NUM_STR_SIZE 20

static char* arduino1 = "0,0";


/*--------------------base_logic--------------------*/

/*---------------number_param_start---------------*/
int send_usr_command(const char *val, const struct kernel_param *kp)
{
    int res = param_set_charp(val, kp);
    if(res == 0)
    {
    	printk(KERN_INFO "//-----Set param: '%s';---//\n", val);
        return 0;
    }
    return -1;
}

int get_mcu_response(char *buffer, const struct kernel_param *kp)
{
    int res = param_get_charp(buffer, kp);
    if(res == 0)
    {
        //FILE *fileX;
    	printk(KERN_INFO "//-----Got param: '%s';---//\n", buffer);

        //fileX = fopen("/dev/ttyUSB0", "w");
        //fprintf(fileX, "%s", buffer);
        //fclose(fileX);
            
        return 0;
    }
    return -1;
}

const struct kernel_param_ops usr_num_ops =
{
    .set = &send_usr_command,
    .get = &get_mcu_response,
};

module_param_cb(arduino1, &usr_num_ops, &arduino1, 0744);
MODULE_PARM_DESC(arduino1, "Parameter to communicate with arduino1 controller");
/*----------------number_param_end----------------*/


static int __init mcu_mod_init(void)
{
    printk(KERN_INFO "mcu-module is loaded");
    return 0;
}

static void __exit mcu_mod_exit(void)
{
    printk(KERN_INFO "mcu-module is unloaded");
}

module_init(mcu_mod_init);
module_exit(mcu_mod_exit);
